# Starter Template: TS - CI/CD - NPM
This repository is meant to work as a starter template for Typescript libs continiously deployed to a public npm audience.

## Configured technologies:
- Gitlab CI/CD
- Commitizen (friendly)
- Commit-lint
- Semantic Release (git, gitlab, npm, release-notes, changelog)
- Typescript
- Jest
- Husky

## Getting started

### Make the repo your own

1. Execute `git clone`
1. Change to an appropriate license
1. Replace placeholders package.json


### Add a Gitlab token to your environment

1. Navigate to your gitlab account settings.
1. Generate a personal access token with **api** and **write_repository** scopes (store value).
1. In your **gitlab project** add a new protected, masked variable of key `GL_TOKEN` and assign it the value of the personal token.

This will allow your build agent to publish releases to your gitlab repository.

### Add an NPM token to your environment

1. Generate an NPM_TOKEN for your npm account.
1. In your **gitlab project** add a new protected, masked variable of key `NPM_TOKEN` and assign it the value of an NPM_TOKEN.

This will allow your build agent to release your package to the public npm package.

### Add a deploy key-pair to your environment

#### Generate an SSH key-pair
1. Get your hands on a shell that has access to _OpenSSH_.
1. Execute `ssh-keygen -t ed25519 -C "<comment>"`.
1. When promtped for a password leave the input empty. 

#### Add the deploy key
1. In your **gitlab project** add a new protected, masked variable of key `SSH_DEPLOY_KEY` and assign it the value of your fresh ssh private key.
1. In your **gitlab project** add a **deploy key** of key name `SSH_DEPLOY_KEY_PUB` (you can name it whatever you want but it's a suitable name).

#### Give the deploy key permission to push to protected branches
As of _2020-04-30_ there's an open [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/30769) in the gitlab repository regarding
deploy keys no longer allowed to push to protected branches were absolutely no-one has push access. 
The issue seems to have been caused by feature collision.

As a workaround you need to allow the user that created the deploy key, I.e you permission to push to the protected branch.
As this might mess up your git workflow allowing you to push un-verified code to your master branch, I've added husky hooks to prohibit pushing code.
This should prohibit pushing by mistake to mess up the master.

Finally adding the deploy keys will allow your build agent to push release commits and related files to the master branch.

## Summary
You should now be able to trigger Typescript deploys by making merge requests towards your master branch.
Happy coding!